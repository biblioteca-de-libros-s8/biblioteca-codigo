#Las operaciones que debes incluir en el menú son las siguientes:
#Agregar Libro: Permite al usuario agregar un nuevo libro a la biblioteca ingresando el título,
#autor y número de ejemplares disponibles, por defecto debes tener registrados al menos 5 libros con sus autores y cantidad de ejemplares.
#Tomar Libro Prestado: Permite a un usuario tomar prestado un libro de la biblioteca. 
#El usuario debe proporcionar el título del libro para obtenerlo. El número de ejemplares disponibles debe reducirse en uno.
#Devolver Libro: Permite a un usuario devolver un libro a la biblioteca. 
#El usuario debe proporcionar el título del libro. El número de ejemplares disponibles debe aumentar en uno.
#Consultar Libros Disponibles: Muestra una lista de todos los libros disponibles en la biblioteca, incluyendo título, autor y número de ejemplares.
#Salir: Termina el programa.
#Define una clase llamada Biblioteca que tenga una estructura de datos (por ejemplo, un diccionario) 
#para mantener un registro de los libros y sus cantidades disponibles, así como métodos para realizar las operaciones mencionadas anteriormente.
#El programa debe implementar un bucle que permita al usuario seleccionar una de las opciones y realizar la operación correspondiente. 
#El programa debe continuar ejecutándose hasta que el usuario elija la opción de salir.

import random
class Biblioteca:
    def __init__(self):
        self.libros = []
    def agregar_libro(self, titulo, autor, ejemplares):
        libro = {"titulo": titulo, "autor": autor, "ejemplares": ejemplares}
        self.libros.append(libro)
        print(f"Libro '{titulo}' agregado a la biblioteca.")

    def tomar_libro_prestado(self, titulo):
        for libro in self.libros:
            if libro["titulo"] == titulo and libro["ejemplares"] > 0:
                libro["ejemplares"] -= 1
                print(f"Ha tomado prestado el libro '{titulo}'.")
                break
        else:
            print(f"El libro '{titulo}' no está disponible.")

    def devolver_libro(self, titulo):
        for libro in self.libros:
            if libro["titulo"] == titulo:
                libro["ejemplares"] += 1
                print(f"Ha devuelto el libro '{titulo}'.")
                break
        else:
            print("No se pudo encontrar el libro en la biblioteca.")
    def consultar_libros_disponibles(self):
        print("\nLibros Disponibles:")
        for libro in self.libros:
            print(f"Título: {libro['titulo']}, Autor: {libro['autor']}, Ejemplares disponibles: {libro['ejemplares']}")
# Instanciamos la función
biblioteca = Biblioteca()
# Agregar libros iniciales
biblioteca.agregar_libro("Drácula", "Bram Stoker", random.randint(1, 10))
biblioteca.agregar_libro("Frankenstein o el eterno prometeo", "Mary Shelley", random.randint(1, 10))
biblioteca.agregar_libro("Cuentos macabros", "Edgar Allan Poe", random.randint(1, 10))
biblioteca.agregar_libro("Otra vuelta de tuerca", "Henry James", random.randint(1, 10))
biblioteca.agregar_libro("En las montañas de la locura", "H. P. Lovecraft", random.randint(1, 10))
while True:
    print("\nMenú:")
    print("1. Agregar Libro")
    print("2. Tomar Libro Prestado")
    print("3. Devolver Libro")
    print("4. Consultar Libros Disponibles")
    print("5. Salir")
    opcion = input("Ingrese el número de la opción que desea: ")
    if opcion == "1":
        titulo = input("Ingrese el título del libro: ")
        autor = input("Ingrese el autor del libro: ")
        ejemplares = int(input("Ingrese el número de ejemplares disponibles: "))
        biblioteca.agregar_libro(titulo, autor, ejemplares)
    elif opcion == "2":
        titulo = input("Ingrese el título del libro que desea tomar prestado: ")
        biblioteca.tomar_libro_prestado(titulo)
    elif opcion == "3":
        titulo = input("Ingrese el título del libro que desea devolver: ")
        biblioteca.devolver_libro(titulo)
    elif opcion == "4":
        biblioteca.consultar_libros_disponibles()
    elif opcion == "5":
        print("Gracias por usar la biblioteca. ¡Hasta luego!")
        break
    else:
        print("Opción no válida. Por favor, elija una opción válida.")


#Rene Israel Palacios Rodriguez #RP1065012023
#Genesis Elizabeth Galdamez Martinez #GM2624012023 
#Andres Felipe Galan Hernandez #GH0836012023
